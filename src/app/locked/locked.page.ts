import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
 
const { BiometricAuth } = Plugins;
 
@Component({
  selector: 'app-locked',
  templateUrl: './locked.page.html',
  styleUrls: ['./locked.page.scss'],
})
export class LockedPage implements OnInit {
  showFallback = true;
  password = '1234';
  hasBiometricAuth = false;
 
  constructor(private modalCtrl: ModalController) { }
 
  async ngOnInit() {     
    const available = await BiometricAuth.isAvailable();
    this.hasBiometricAuth = available.has;
    if (this.hasBiometricAuth) {
      this.openBiometricAuth();
    }
  }
 
  dismissLockScreen() {
    this.modalCtrl.dismiss({reset: true});
  }
  
  async openBiometricAuth() {
    const authResult = await BiometricAuth.verify(
      {
        reason: 'Use Biometric to unlock',
        title: 'Use Biometric to unlock',
      }
    );      
    if (authResult.verified) {
      this.dismissLockScreen();
    } 
  }
 
  unlock() {
    if (this.password == '1234') {
      this.dismissLockScreen();
    }
  }
 
 
}
